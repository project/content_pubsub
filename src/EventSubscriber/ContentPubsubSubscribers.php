<?php

/**
 * Content PubSub Event subscriber.
 */

namespace Drupal\content_pubsub\EventSubscriber;

use Drupal\core_event_dispatcher\Event\Entity\EntityViewEvent;
use Drupal\core_event_dispatcher\Event\Entity\EntityDeleteEvent;
use Drupal\core_event_dispatcher\Event\Entity\EntityInsertEvent;
use Drupal\core_event_dispatcher\Event\Entity\EntityUpdateEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;
use Drupal\redis\Client\PhpRedis;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ContentPubsubSubscribers implements EventSubscriberInterface
{
  private $redis;

  function __construct()
  {
    $this->redis = new PhpRedis();
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array
  {
    return [
      HookEventDispatcherInterface::ENTITY_VIEW => 'entityView',
      HookEventDispatcherInterface::ENTITY_INSERT => 'entityInsert',
      HookEventDispatcherInterface::ENTITY_UPDATE => 'entityUpdate',
      HookEventDispatcherInterface::ENTITY_DELETE => 'entityDelete',
    ];
  }

  /**
   * Entity view.
   *
   * @param \Drupal\core_event_dispatcher\Event\Entity\EntityViewEvent $event
   *   The event.
   */
  public function entityView(EntityViewEvent $event): void
  {
    $entity = $event->getEntity();
  }

  /**
   * Entity insert.
   *
   * @param \Drupal\core_event_dispatcher\Event\Entity\EntityInsertEvent $event
   *   The event.
   */
  public function entityInsert(EntityInsertEvent $event): void
  {
    $entity = $event->getEntity();

    $host = \Drupal::request()->getSchemeAndHttpHost();
    $bundle = $entity->bundle();
    $uuid = $entity->get('uuid')->value;
    $url = "{$host}/api/node/{$bundle}/{$uuid}";

    $message = json_encode(array(
      "type" => "page",
      "url" => $url,
    ));

    $this->redis->getClient('redis')
      ->publish('content:insert', $message);
  }

  /**
   * Entity update.
   *
   * @param \Drupal\core_event_dispatcher\Event\Entity\EntityUpdateEvent $event
   *   The event.
   */
  public function entityUpdate(EntityUpdateEvent $event): void
  {
    $entity = $event->getEntity();
    $bundle = $entity->bundle();

    if ($bundle !== 'content_moderation_state') {
      $host = \Drupal::request()->getSchemeAndHttpHost();
      $uuid = $entity->get('uuid')->value;
      $url = "{$host}/api/node/{$bundle}/{$uuid}";

      $message = json_encode(array(
        "type" => "page",
        "url" => $url,
      ));

      $this->redis->getClient('redis')
        ->publish('content:update', $message);
    }
  }

  /**
   * Entity delete.
   *
   * @param \Drupal\core_event_dispatcher\Event\Entity\EntityDeleteEvent $event
   *   The event.
   */
  public function entityDelete(EntityDeleteEvent $event): void
  {
    $entity = $event->getEntity();

    $host = \Drupal::request()->getSchemeAndHttpHost();
    $bundle = $entity->bundle();
    $uuid = $entity->get('uuid')->value;
    $url = "{$host}/api/node/{$bundle}/{$uuid}";

    $message = json_encode(array(
      "type" => "page",
      "url" => $url,
    ));

    $this->redis->getClient('redis')
      ->publish('content:delete', $message);
  }
}
